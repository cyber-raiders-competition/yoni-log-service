
# Yoni Logging Service

## Build Instructions

```
python3 -m virtualenv .
source bin/activate
pip3 install -r requirements.txt
```

## Run Server

```
python manage.py runserver -h 0.0.0.0 -p 8080
```

## Make Test Post

```
curl -H "Content-Type: application/json" \
    -X POST \
    -d '{"api_token":"311ca9b0-2b9e-4a16-ab17-df2e8bfa7aed"}' \
    http://localhost:8080/api/log/report
```

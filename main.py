
import subprocess
from flask import Flask, request, jsonify
import platform
import psutil

app = Flask(__name__)

API_TOKENS = [
    "311ca9b0-2b9e-4a16-ab17-df2e8bfa7aed"
]

@app.route("/api/log/report", methods = ['POST'])
def log_report():
    api_token = request.json.get('api_token')
    
    if api_token in API_TOKENS:
        return jsonify({
            "code": 200,
            "log": {
                "report": {
                    "system": platform.system(),
                    "release": platform.release(),
                    "version": platform.version(),
                    "cpu_count": psutil.cpu_count(),
                    "ram_count": psutil.virtual_memory().total,
                    "cpu_usage": psutil.cpu_percent(interval=1),
                    "ram_usage": psutil.virtual_memory().used,
                    "disk_free_percent": psutil.disk_usage('/').percent,
                    "services": [
                        (serv.laddr, serv.status, psutil.Process(serv.pid).exe())
                        for serv in psutil.net_connections()
                        if serv.status == "LISTEN"
                    ]
                }
            }
        })
    else:
        return jsonify({
            "code": 401,
            "error": [
                "Access Denied"    
            ]
        })
